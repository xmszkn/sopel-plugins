# coding=utf-8
# https://pypi.org/project/sopel/
# Author xmszkn(at)disroot.org
# GPLv3 license
# title: skrypt papieski

from datetime import datetime, timedelta
from sopel import plugin

last_sent_date = None  # Data ostatniego wysłania wiadomości
already_sent = None

@plugin.interval(2)
def p2157(bot):
    global last_sent_date  # Umożliwia modyfikację zmiennej globalnej
    global already_sent
    
    now = datetime.now()

    # Sprawdzenie, czy to nowy dzień
    if last_sent_date is None or last_sent_date.date() != now.date():
        # Resetowanie zmiennych dla nowego dnia
        last_sent_date = now
        already_sent = False

    # Wysłanie wiadomości tylko raz na dzień o godzinie 21:37
    if already_sent is False and now.hour == 21 and now.minute == 37:
        bot.say("2137", recipient="#python")
        already_sent = True
