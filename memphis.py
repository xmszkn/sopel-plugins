# coding=utf-8
# https://pypi.org/project/sopel/
# Author xmszkn(at)disroot.org
# GPLv3 license

from sopel import plugin, tools # sopel modules
from random import choice, choices
import os

@plugin.command('memphis', 'm', 'memph')
@plugin.action_command('mephis')
@plugin.example('.mephis', r'memphis')

def cytat(bot, trigger):
    query = trigger.group(2)
    if not query:
    # list with phrases from file about memphis
        current_directory = os.path.dirname(os.path.abspath(__file__))
        file_path = os.path.join(current_directory, 'hejty.txt')
        with open(file_path, 'r') as plik:
            hejty_1 = plik.readlines()
    # random choice from list
        hejty_2 = choices(hejty_1, k=4)
        hejty_2 = [element.strip() for element in hejty_2]

        last_element = bot.memory.get('last_element')
        answer = None

        while True:
            answer = choice(hejty_2)
            if answer != last_element:
                break

        bot.memory['last_element'] = answer
        bot.say(answer)

    if query == "add":
        bot.reply("Memy o memphisie dodajemy i uzgodniamy na priv z moim panem wladcą albo tajemniczym wielbicielem")
