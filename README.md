### Repo with plugins for pybot irc bot on [#knajpa](https://rentry.co/polska-knajpa) on [pirc.pl](https://pirc.pl) network
### Software [Sopel](https://sopel.chat/) Irc bot written in python
### Installation:

Install dependecies modules, just pip install <module>
(https://pypi.org/project/)  
Put <plugin>.py to your_path_to_sopel/modules   
Hard restart your bot.  

Author or subauthor xmszkn
License GPLv3.0

### List of plugins:

- cytat.py 	Plugin that extracts a quote from wikiquote, in Polish or English
- memphis.py  Reads jokes about Memphis monkey from file
- pogoda-req.py  Same but safe and recommended
- stats.py 	Stastics based on channel logs
- urbandict.py Definitions from urbandictionary, authored by mutantmonkey NEW!
- piped.py Change youtube url to piped.video

Rest plugins are available in testing branch

### planned

Do you have suggestions? Let me know
Make pull request
